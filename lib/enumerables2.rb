require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.inject(0, &:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |string| string =~ Regexp.new(substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  chars_with_multiple_instances(string.chars).sort
end

def chars_with_multiple_instances(chars)
  non_uniques = Set.new
  chars.each do |char|
    non_uniques.add(char) if char != ' ' && string.count(char) > 1
  end
  non_uniques.to_a
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = remove_punctuation(string).split

  words.reduce(['', '']) do |longest, word|
    if word.length >= longest[0].length
      longest.unshift(word)
      longest.delete_at(2)
    end
    longest
  end
end

def remove_punctuation(str)
  str.delete('\'"?,!.')
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ('a'..'z').to_a.join('').delete(string).split('')
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).to_a.delete_if { |year| !not_repeat_year?(year) }
end

def not_repeat_year?(year)
  year.to_s.split('').uniq.length == 4
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  delete_repeats(songs).uniq
end

def delete_repeats(songs)
  songs.delete_if { |song| !no_repeats?(song, songs) }
end

def no_repeats?(song_name, songs)
  last_index = songs.index(song_name)

  songs[last_index..-1].each_with_index do |song, index|
    if song == song_name
      return false if last_index - index == -1
      last_index = index
    end

  end

  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  sentence = remove_punctuation(string)
  c_words = sentence.split.select { |word| word.index('c') }
  c_distances = c_words.map { |word| c_distance(word) }
  c_words[c_distances.min]
end

def c_distance(word)
  word.length - word.rindex('c') - 1
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  ranges = []

  arr.each_with_index do |el, i|
    range = [i, i]

    arr[i..-1].each_with_index do |el2, j|
      break if el2 != el
      range[1] = j + i

    end

    ranges.push range if range[0] != range[1]
  end

  ranges.uniq { |range| range[1] }
end
